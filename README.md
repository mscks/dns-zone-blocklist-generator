
[![GitLab CI Pipeline Status](https://gitlab.com/mscks/dns-zone-blocklist-generator/badges/main/pipeline.svg)](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/pipelines)

# DNS Zone blocklist Generator

This project generates a zone file for [BIND](https://en.wikipedia.org/wiki/BIND), [Dnsmasq](https://en.wikipedia.org/wiki/Dnsmasq) and [Unbound](https://en.wikipedia.org/wiki/Unbound_(DNS_server)) DNS servers using data from the [StevenBlack/hosts](https://github.com/StevenBlack/hosts) project. The generated zone files can be used to block ads and malware for an entire network when used with a local DNS server.

DNS based ad blockers can support wildcard entries. This tool filters out any subdomains of known adware or malware domains, reducing the number of zone entries required from **157,803** down to **101,073**.

| DNS Server | Response Type | Download  | SHA256 Checksum |
| ---------- |:-------------:|:---------:|:---------------:|
| BIND | 0.0.0.0 | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/bind.zones.blocklist) | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/bind.zones.blocklist.checksum) |
| BIND (RPZ) | NXDOMAIN | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/bind.nxdomain.blocklist) | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/bind.nxdomain.blocklist.checksum) |
| Dnsmasq | 0.0.0.0 | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/dnsmasq.blocklist) | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/dnsmasq.blocklist.checksum) |
| Dnsmasq | NXDOMAIN | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/dnsmasq.server.blocklist) | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/dnsmasq.server.blocklist.checksum) |
| Unbound | 0.0.0.0 | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/unbound.blocklist) | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/unbound/unbound.blocklist.checksum) |
| Unbound | NXDOMAIN | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/unbound.nxdomain.blocklist) | [link](https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/unbound.nxdomain.blocklist.checksum) |

## Blocklist Updates

The blocklists are updated every 24 hours with the latest data from [StevenBlack/hosts](https://github.com/StevenBlack/hosts). The builds logs are publicly available on [Travis CI](https://travis-ci.org/oznu/dns-zone-blocklist) and each zone file is tested to be valid before publishing.

The compiled blocklist files will be saved to the `./output` a directories in the root of the project.

### Custom Entries

Custom entries can be added to the custom.blocklist.json file in the root of this project before building.

### Allowlist

Any domains you wish to exclude from the blocklist can be added to the custom.allowlist.json file in the root of this project before building.


https://gitlab.com/mscks/dns-zone-blocklist-generator/-/raw/main/output/bind.bind-nxdomain.blocklist
